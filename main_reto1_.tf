# Especificación de proveedor cloud y versión
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.0"
    }
  }
}

# Configuración de proveedor Microsoft Azure Cloud
provider "azurerm" {
  features {}
}

# Creación de resource group
resource "azurerm_resource_group" "copa-rg" {
  name     = "copa-rg"
  location = "South Central US"
  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación de App Service Plan
resource "azurerm_service_plan" "plan-copa-macintoshenjoyers" {
  name                = "plan-copa-macintoshenjoyers"
  resource_group_name = azurerm_resource_group.copa-rg.name
  location            = azurerm_resource_group.copa-rg.location
  os_type             = "Windows"
  sku_name            = "S1"
  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

# Creación de App Service (Web App)
resource "azurerm_windows_web_app" "app-copa-macintoshenjoyers" {
  name                = "app-copa-macintoshenjoyers"
  resource_group_name = azurerm_resource_group.copa-rg.name
  location            = azurerm_service_plan.plan-copa-macintoshenjoyers.location
  service_plan_id     = azurerm_service_plan.plan-copa-macintoshenjoyers.id
  https_only = true
  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }

  site_config {
    minimum_tls_version = "1.2"
  }

  # Conexión de App Service con Application Insights
  app_settings = {
    "APPINSIGHTS_INSTRUMENTATIONKEY" = "${azurerm_application_insights.appi-copa-macintoshenjoyers.instrumentation_key}"
  }
}

# Conexión de App Service con repositorio de código fuente
resource "azurerm_app_service_source_control" "app-sc-copa-macintoshenjoyers" {
  app_id   = azurerm_windows_web_app.app-copa-macintoshenjoyers.id
  repo_url = "https://github.com/iukion/CopaHackathon_html_base"
  branch   = "master"
  use_manual_integration = true
  use_mercurial = false
}

# Creación de Application Insights
resource "azurerm_application_insights" "appi-copa-macintoshenjoyers" {
  name                = "appi-copa-macintoshenjoyers"
  location            = azurerm_resource_group.copa-rg.location
  resource_group_name = azurerm_resource_group.copa-rg.name
  application_type    = "web"

  # Conexión de Application Insights con Log Analytics Workspace  
  workspace_id = azurerm_log_analytics_workspace.log-copa-macintoshenjoyers.id

  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}

output "instrumentation_key" {
  value = azurerm_application_insights.appi-copa-macintoshenjoyers.instrumentation_key
  sensitive = true
}

output "app_id" {
  value = azurerm_application_insights.appi-copa-macintoshenjoyers.app_id
}

# Creación de Log Analytics Workspace
resource "azurerm_log_analytics_workspace" "log-copa-macintoshenjoyers" {
  name                = "log-copa-macintoshenjoyers"
  location            = azurerm_resource_group.copa-rg.location
  resource_group_name = azurerm_resource_group.copa-rg.name
  sku                 = "PerGB2018"
  retention_in_days   = 30
  tags = {
    "CREATEDBY" = "Macintosh Enjoyers",
    "DPT" = "VENTAS",
    "AMBIENTE" = "PRD"
  }
}
